<?php
use Drupal\Core\Database\Database;
/**
 * Implment hook_schema()
 */
function ims_schema() {
  // Create database table called 'ims_user_invoice' with the following colums:
  // 1) id [serial], 2) uid [int], 3) name [varchar], 4)rfc [varchar],
  // 5) postal_code [varchar], 6) tax_regime [int], 7) cfdi [varchar],
  // 8) mail [varchar], 9) address [int], 10) number_ext [varchar], 11) number_int [varchar],
  // 12) neighborhood [varchar], 13) municipality [varchar], 14) city [varchar], 15) state [varchar]
  $schema['ims_user_invoice'] = [
    'description' => 'User billing information.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'ID invoice data.',
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'User ID connected with invoice data.',
      ],
      'name' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
        'description' => 'Name or Social reason.',
      ],
      'rfc' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
        'description' => 'User rfc.',
      ],
      'postal_code' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 5,
        'description' => 'Postal Code.',
      ],
      'tax_regime' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Fiscal regime.',
      ],
      'cfdi' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
        'description' => 'Use of CFDI.',
      ],
      'mail' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 100,
        'description' => 'Users email to send the invoice.',
      ],
      'address' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
        'description' => 'Name of the street.',
      ],
      'number_ext' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 50,
        'description' => 'Exterior address number.',
      ],
      'number_int' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 50,
        'description' => 'Interior address number.',
      ],
      'neighborhood' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
        'description' => 'Neighborhood Name.',
      ],
      'municipality' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
        'description' => 'Municipality Name.',
      ],
      'city' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
        'description' => 'City name.',
      ],
      'state' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
        'description' => 'State Name.',
      ]
    ],
    'primary key' => ['id'],
  ];

  // Create database table called 'ims_invoices' with the following colums:
  // 1) id [serial], 2) sid [int], 3) folio [int], 4)uuid [varchar],
  // 5) pdf [varchar], 6) xml [int], 7) created [varchar],
  // 8) cancelled [varchar], 9) p_general [int]
  $schema['ims_invoices'] = [
    'description' => 'Details of invoices generated',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for the record.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sid' => [
        'description' => 'The sale id.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'folio' => [
        'description' => 'Folio',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'uuid' => [
        'description' => 'UUID.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ],
      'pdf' => [
        'description' => 'Url pdf.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 100,
      ],
      'xml' => [
        'description' => 'Url xml.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 100,
      ],
      'created' => [
        'description' => 'Timestamp for when invoice was created.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 100,
      ],
      'cancelled' => [
        'description' => 'If invoice was cancelled',
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
      ],
      'p_general' => [
        'description' => 'Type invoice',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id']
  ];
  return $schema;
}
