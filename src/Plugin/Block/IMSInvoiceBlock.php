<?php

namespace Drupal\ims\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a invoicing block with all needed fields.
 *
 * @Block(
 *   id = "invoice_block",
 *   admin_label = @Translation("IMS Invoicing Block"),
 * )
 */
class IMSInvoiceBlock extends BlockBase implements ContainerFactoryPluginInterface{

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
  */
  protected $database;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current route match.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Drupalist object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current_user.
   * @param \Drupal\Core\Database\Connection $connection
   *  The database connection service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *  A request stack symfony instance.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */

  public function __construct(
    array $configuration, 
    $plugin_id, 
    $plugin_definition, 
    AccountInterface $currentUser,
    Connection $connection,
    RequestStack $requestStack,
    RouteMatchInterface $routeMatch,
    ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $currentUser;
    $this->database = $connection;
    $this->requestStack = $requestStack;
    $this->routeMatch = $routeMatch;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ){
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('config.factory'),
    );
  }

  /**
  * {@inheritdoc}
  */
  public function build() {
    $note = $this->t('If you do not invoice at the time of purchase, you have 
    until the penultimate day of month to generate it, otherwise, is generated 
    as an invoice for the General Public.');

    if ($this->currentUser->isAuthenticated()) {
      // Get current user billing data
      $current_user = $this->database->select('ims_user_invoice', 'i')
        ->condition('uid', $this->currentUser->id())
        ->fields('i')
        ->execute()
        ->fetchAssoc();
      if ($current_user) {
        // Get the user's last purchase
        $sale_result = $this->database->select('ppss_sales', 's')
          ->condition('uid', $this->currentUser->id())
          ->fields('s', ['id','details'])
          ->orderBy('created', 'DESC')
          ->execute()
          ->fetchAll();
        $url = Url::fromRoute('ims.invoice', ['id' => $sale_result[0]->id]);
        $description = '<br>'.$this->t('Click the button to generate your 
        invoice') . '<br><br><b>Note: </b>' . $note;
      }
      else {
        $url = Url::fromRoute('ims.user_data_form', [
          'user' => $this->currentUser->id()
        ]);
        $description = '<br>'.$this->t('Please register your tax information as 
        it appears on your <a href=@url target="_blank">Constancia de Situación 
        Fiscal</a> to generate your invoice.', [
          '@url' => "https://www.sat.gob.mx/aplicacion/53027/genera-tu-constancia-de-situacion-fiscal"
        ]).'<br><br><b>Note: </b>'.$note;
      }
      $data['title'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<h3>Billing Information</h3>')
      ];
      $data['url'] = [
        '#type' => 'link',
        '#title' => $this->t($current_user ?
          'Generate invoice' : 'Enter billing information'),
        '#url' => $url,
        '#attributes' => [
          'class' => [
            'button',
          ],
        ]
      ];
      $data['description'] = [
        '#type' => 'markup',
        '#markup' => $description
      ];
      return $data;
    }
    else {
      return [
        '#markup' => '<h5>'.$this->t('Billing details') . '</h5>
        '.$this->t('To generate your invoice you need to <a href="/user/login">
        Log in</a> and enter your tax information in billing tab, As it appear 
        in your "Constancia de Situación Fiscal"') . '<br><br><b>Note: </b>' . $note
      ];
    }
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account)
  {
    // If viewing a node, get the fully loaded node object.
    $node = $this->routeMatch->getParameter('node');

    if (!(is_null($node))) {
      $request_uri = $this->requestStack->getCurrentRequest()->getRequestUri();

      if (strchr($request_uri, $this->configFactory->get('ppss.settings')->get('success_url'))) {
        return AccessResult::allowedIfHasPermission($account, 'view ims block');
      }
      
    }

    return AccessResult::forbidden();
  }
  
}