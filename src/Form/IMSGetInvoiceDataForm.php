<?php

namespace Drupal\ims\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides IMS get invoice data form.
 */
class IMSGetInvoiceDataForm extends FormBase {

    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a database connection.
   */
  public function __construct(
    Connection $database,
  ) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
  ){
    return new static(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ims_invoice_data_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL) {
    $user_id = $this->currentUser()->hasPermission('access user profiles') ? $user : $this->currentUser()->id();
    // Get user billing data
    $current_user = $this->database->select('ims_user_invoice', 'i')
      ->condition('uid', $user_id)
      ->fields('i')
      ->execute()
      ->fetchAssoc();

    $form['description'] = [
      '#markup' => '<b>Note:</b> '.$this->t('Make sure you have valid tax 
      information, which you can get from "Constancia de Situación Fiscal" 
      or "Cédula de Identificación Fiscal CIF".')
    ];
    $form['id'] = [
      '#type' => 'hidden',
      '#default_value' => $user_id,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name or Company name'),
      '#required' => TRUE,
      '#default_value' => $current_user['name'] ?? '',
      '#description' => $this->t('As it appear in your "Constancia de 
        Situación Fiscal"')
    ];
    $form['rfc'] = [
      '#type' => 'textfield',
      '#title' => 'RFC',
      '#required' => TRUE,
      '#default_value' => $current_user['rfc'] ?? '',
      '#description' => $this->t('You must enter each letter or number of 
        the RFC. As it appear in your "Constancia de Situación Fiscal"')
    ];
    $form['tax_regime'] = [
      '#type' => 'select',
      '#title' => $this->t('Tax Regime'),
      '#options' => [
        '601' => '601-General de Ley Personas Morales',
        '603' => '603-Personas Morales con Fines no Lucrativos',
        '605' => '605-Sueldos y Salarios e Ingresos Asimilados a Salarios',
        '606' => '606-Arrendamiento',
        '607' => '607-Régimen de Enajenación o Adquisición de Bienes',
        '608' => '608-Demás ingresos',
        '610' => '610-Residentes en el Extranjero sin Establecimiento Permanente en México',
        '611' => '611-Ingresos por Dividendos (socios y accionistas)',
        '612' => '612-Personas Físicas con Actividades Empresariales y Profesionales',
        '614' => '614-Ingresos por intereses',
        '615' => '615-Régimen de los ingresos por obtención de premios',
        '616' => '616-Sin obligaciones fiscales',
        '620' => '620-Sociedades Cooperativas de Producción que optan por diferir sus ingresos',
        '621' => '621-Incorporación Fiscal',
        '622' => '622-Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras',
        '623' => '623-Opcional para Grupos de Sociedades',
        '624' => '624-Coordinados',
        '625' => '625-Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas',
        '626' => '626-Régimen Simplificado de Confianza',
      ],
      '#required' => TRUE,
      '#default_value' => $current_user['tax_regime'] ?? '',
      '#description' => $this->t('As it appear in your "Constancia de Situación Fiscal"')
    ];
    $form['cfdi'] = [
      '#type' => 'select',
      '#title' => $this->t('Use of CFDI'),
      '#options' => [
        'G01' => 'G01-Adquisición de mercancías',
        'G02' => 'G02-Devoluciones, descuentos o bonificaciones',
        'G03' => 'G03-Gastos en general',
        'I01' => 'I01-Construcciones',
        'I02' => 'I02-Mobiliario y equipo de oficina por inversiones',
        'I03' => 'I03-Equipo de transporte',
        'I04' => 'I04-Equipo de cómputo y accesorios',
        'I05' => 'I05-Dados, troqueles, moldes, matrices y herramental',
        'I06' => 'I06-Comunicaciones telefónicas',
        'I07' => 'I07-Comunicaciones satelitales',
        'I08' => 'I08-Otra maquinaria y equipo',
        'D01' => 'D01-Honorarios médicos, dentales y gastos hospitalarios',
        'D02' => 'D02-Gastos médicos por incapacidad o discapacidad',
        'D03' => 'D03-Gastos funerales',
        'D04' => 'D04-Donativos',
        'D05' => 'D05-Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)',
        'D06' => 'D06-Aportaciones voluntarias al SAR',
        'D07' => 'D07-Primas por seguros de gastos médicos',
        'D08' => 'D08-Gastos de transportación escolar obligatoria',
        'D09' => 'D09-Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones',
        'D10' => 'D10-Pagos por servicios educativos (colegiaturas)',
        'CP01' => 'CP01-Pagos',
        'CN01' => 'CN01-Nómina',
        'S01' => 'S01-Sin Efectos Fiscales',
      ],
      '#required' => TRUE,
      '#default_value' => $current_user['cfdi'] ?? 'G03',
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#default_value' => $current_user['mail'] ?? $this->currentUser()->getEmail(),
      '#description' => $this->t('Valid email to receive your invoice')
    ];
    $form['address'] = [
      '#type' => 'details',
      '#title' => $this->t('Address information'),
      '#open' => TRUE
    ];
    $form['address']['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal Code'),
      '#required' => TRUE,
      '#default_value' => $current_user['postal_code'] ?? '',
      '#description' => $this->t('Postal code of your tax address')
    ];
    $form['address']['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street Name'),
      '#required' => TRUE,
      '#default_value' => $current_user['address'] ?? '',
    ];
    $form['address']['number_ext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Exterior address number'),
      '#required' => TRUE,
      '#default_value' => $current_user['number_ext'] ?? '',
    ];
    $form['address']['number_int'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interior address number'),
      '#required' => FALSE,
      '#default_value' => $current_user['number_int'] ?? '',
    ];
    $form['address']['neighborhood'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Neighborhood Name'),
      '#required' => FALSE,
      '#default_value' => $current_user['neighborhood'] ?? '',
    ];
    $form['address']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City name'),
      '#required' => TRUE,
      '#default_value' => $current_user['city'] ?? '',
    ];
    $form['address']['municipality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Municipality name'),
      '#required' => TRUE,
      '#default_value' => $current_user['municipality'] ?? '',
    ];
    $form['address']['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State name'),
      '#required' => TRUE,
      '#options' => [
        'AGUASCALIENTES' => 'AGUASCALIENTES',
        'BAJA CALIFORNIA' => 'BAJA CALIFORNIA',
        'BAJA CALIFORNIA SUR' => 'BAJA CALIFORNIA SUR',
        'CAMPECHE' => 'CAMPECHE',
        'COAHUILA' => 'COAHUILA',
        'COLIMA' => 'COLIMA',
        'CHIAPAS' => 'CHIAPAS',
        'CHIHUAHUA' => 'CHIHUAHUA',
        'CIUDAD DE MEXICO' => 'CIUDAD DE MEXICO',
        'DURANGO' => 'DURANGO',
        'GUANAJUATO' => 'GUANAJUATO',
        'GUERRERO' => 'GUERRERO',
        'HIDALGO' => 'HIDALGO',
        'JALISCO' => 'JALISCO',
        'MEXICO' => 'MEXICO',
        'MICHOACAN' => 'MICHOACAN',
        'MORELOS' => 'MORELOS',
        'NAYARIT' => 'NAYARIT',
        'NUEVO LEON' => 'NUEVO LEON',
        'OAXACA' => 'OAXACA',
        'PUEBLA' => 'PUEBLA',
        'QUERETARO' => 'QUERETARO',
        'QUINTANA ROO' => 'QUINTANA ROO',
        'SAN LUIS POTOSI' => 'SAN LUIS POTOSI',
        'SINALOA' => 'SINALOA',
        'SONORA' => 'SONORA',
        'TABASCO' => 'TABASCO',
        'TAMAULIPAS' => 'TAMAULIPAS',
        'TLAXCALA' => 'TLAXCALA',
        'VERACRUZ' => 'VERACRUZ',
        'YUCATAN' => 'YUCATAN',
        'ZACATECAS' => 'ZACATECAS',
      ],
      '#default_value' => $current_user['state'] ?? '',
    ];
    $form['note'] = [
      '#type' => 'markup',
      '#markup' => '<b>NOTE: </b>'.$this->t('IF YOU DO NOT PROVIDE THE INFORMATION, THE INVOICE CANNOT BE 
      GENERATED') . '<br>'
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $rfc = $form_state->getValue('rfc');
    $cp = $form_state->getValue('postal_code');
    $tax_regime = $form_state->getValue('tax_regime');
    $cfdi = $form_state->getValue('cfdi');
    $email = $form_state->getValue('email');
    $id_user = $this->currentUser()->hasPermission('access user profiles') ? $form_state->getValue('id') : $this->currentUser()->id();
    // Address information
    $address = $form_state->getValue('address');
    $number_ext = $form_state->getValue('number_ext');
    $number_int = $form_state->getValue('number_int');
    $neighborhood = $form_state->getValue('neighborhood');
    $city = $form_state->getValue('city');
    $municipality = $form_state->getValue('municipality');
    $state = $form_state->getValue('state');

    $num_rows = $this->database->select('ims_user_invoice', 'i')
      ->condition('uid', $id_user)->fields('i')
      ->countQuery()
      ->execute()
      ->fetchField();

    if( $num_rows > 0) {
      // Update data
      $this->database->update('ims_user_invoice')->fields([
        'name' => $name,
        'rfc' => $rfc,
        'postal_code' => $cp,
        'tax_regime' => $tax_regime,
        'cfdi' => $cfdi,
        'mail' => $email,
        'address' => $address,
        'number_ext' => $number_ext,
        'number_int' => $number_int,
        'neighborhood' => $neighborhood,
        'city' => $city,
        'municipality' => $municipality,
        'state' => $state,
      ])->condition('uid', $id_user, '=')->execute();

      $this->messenger()->addMessage($this->t('Your changes have been successfully saved.'));
    }
    else {
      // Insert data
      $this->database->insert('ims_user_invoice')->fields([
        'uid' => $id_user,
        'name' => $name,
        'rfc' => $rfc,
        'postal_code' => $cp,
        'tax_regime' => $tax_regime,
        'cfdi' => $cfdi,
        'mail' => $email,
        'address' => $address,
        'number_ext' => $number_ext,
        'number_int' => $number_int,
        'neighborhood' => $neighborhood,
        'city' => $city,
        'municipality' => $municipality,
        'state' => $state,
      ])->execute();

      $this->messenger()->addMessage($this->t('Your information have been successfully saved.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^[A-Z0-9ÑÁÉÍÓÚ ]+$/', $form_state->getValue('name'))) {
      $form_state->setErrorByName('name', $this->t('The Name or Company Name 
      must be in capital letters and not have special characters.'));
    }
    if (strlen($form_state->getValue('rfc')) < 12 || strlen($form_state->getValue('rfc')) > 13) {
      $form_state->setErrorByName('rfc', $this->t('RFC must be 12 or 13 characters.'));
    }
    if(strlen($form_state->getValue('postal_code')) != 5) {
      $form_state->setErrorByName('postal_code', $this->t('Postal code must be 5 characters.'));
    }
  }

}
