<?php

namespace Drupal\ims\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides IMS configuration form.
 */
class IMSAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ims.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ims_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ims.settings');

    $form['ims_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('IMS settings'),
      '#open' => TRUE,
      '#description' => $this->t('Before proceed, first creates an account at 
      @factura_digital to obtain all values of this form.',[
        '@factura_digital' => Link::fromTextAndUrl('Factura Digital', 
        Url::fromUri('https://app.facturadigital.com.mx/registro', [
          'attributes' => [
            'onclick' => 'target="_blank"',
          ],
        ]))->toString(),
      ]),
    ];
    $form['ims_settings']['api_endpoint'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('api_endpoint'),
      '#title' => 'Endpoint'
    ];
    $form['ims_settings']['api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['ims_settings_invoice'] = [
      '#type' => 'details',
      '#title' => $this->t('Invoicing service data'),
      '#open' => TRUE
    ];
    $form['ims_settings_invoice']['tax_regime'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('tax_regime'),
      '#title' => $this->t('Tax Regime')
    ];
    $form['ims_settings_invoice']['expedition_place'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('expedition_place'),
      '#title' => $this->t('Expedition place'),
      '#description' => $this->t('Enter the postal code of expedition place'),
    ];
    $form['ims_settings_invoice']['serie'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('serie'),
      '#title' => $this->t('Series')
    ];
    $form['ims_settings_invoice']['folio'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('folio'),
      '#title' => $this->t('Folio')
    ];
    $form['ims_settings_invoice']['c_pago'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment Method'),
      '#required' => TRUE,
      '#options' => [
        '01' => '01 Efectivo',
        '02' => '02 Cheque nominativo',
        '03' => '03 Transferencia electrónica de fondos',
        '04' => '04 Tarjeta de crédito',
        '05' => '05 Monedero electrónico',
        '06' => '06 Dinero electrónico',
        '08' => '08 Vales de despensa',
        '12' => '12 Dación en pago',
        '13' => '13 Pago por subrogación',
        '14' => '15 Pago por consignación',
        '15' => '15 Condonación',
        '17' => '17 Compensación',
        '23' => '23 Novación',
        '24' => '24 Confusión',
        '25' => '14 Remisión de deuda',
        '26' => '16 Prescripción o caducidad',
        '27' => '17 A satisfacción del acreedor',
        '28' => '18 Tarjeta de débito',
        '29' => '19 Tarjeta de servicios',
        '30' => '30 Aplicación de anticipos',
        '31' => '31 Intermediario pagos',
        '99' => '99 Por definir',
      ],
      '#default_value' => $config->get('c_pago'),
      '#description' => $this->t('How the payment are made.'),
    ];

    $form['ims_settings_cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron settings'),
      '#open' => TRUE
    ];
    $form['ims_settings_cron']['num_rows'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('num_rows'),
      '#title' => $this->t('Records number'),
      '#description' => $this->t('Number of records to be billed each time the cron is executed.')
    ];
    $form['ims_settings_cron']['stamp_date'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('stamp_date'),
      '#title' => $this->t('Invoicing time interval'),
      '#description' => $this->t('Time interval in days before end of month to 
        invoice general public sales. Example: -1 days, -2 days, -1 week'),
    ];
    $form['ims_settings_cron']['time'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('time'),
      '#title' => $this->t('Hour'),
      '#description' => $this->t('Set the default time that will be used to 
        start General Public Billing. Use this format H:i:s (23:00:00)'),
    ];

    // Start fields configuration.
    $form['fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Fields names settings'),
      '#open' => true,
    ];
    $form['fields']['description'] = [
      '#markup' => $this->t('You always need to add one field of this type in 
        yourcustom node type to generate invoices.'),
    ];
    $form['fields']['field_prod_serv_code'] = [
      '#title' => $this->t('Product or service code field name'),
      '#type' => 'textfield',
      '#default_value' => $config->get('field_prod_serv_code'),
      '#description' => $this->t('What is the internal Drupal system name of the
        field to store product or service code SAT. Example: "field_prod_serv_code".'),
      '#required' => true,
    ];
    $form['fields']['field_unit_code'] = [
      '#title' => $this->t('Unit code field name'),
      '#type' => 'textfield',
      '#default_value' => $config->get('field_unit_code'),
      '#description' => $this->t('What is the internal Drupal system name of the
        field to store unit code SAT. Example: "field_unit_code".'),
      '#required' => true,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ims.settings');
    $config
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('tax_regime', $form_state->getValue('tax_regime'))
      ->set('expedition_place', $form_state->getValue('expedition_place'))
      ->set('serie', $form_state->getValue('serie'))
      ->set('folio', $form_state->getValue('folio'))
      ->set('num_rows', $form_state->getValue('num_rows'))
      ->set('stamp_date', $form_state->getValue('stamp_date'))
      ->set('c_pago', $form_state->getValue('c_pago'))
      ->set('time', $form_state->getValue('time'))
      ->set('field_prod_serv_code', $form_state->getValue('field_prod_serv_code'))
      ->set('field_unit_code', $form_state->getValue('field_unit_code'))
      ->save();
      
    parent::submitForm($form, $form_state);
  }

}
