<?php

namespace Drupal\ims;

use Drupal\Core\Database\Connection;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Class ApiService.
 */
class IMSApiService {

  /**
   * Database connection.
   *
   * Drupal\Core\Database\Connection
  */
  protected $database;

  /**
   * GuzzleHttp client.
   *
   * @var \GuzzleHttp\ClientInterface
  */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
  */
  protected $configFactory;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
  */
  protected $loggerFactory;

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;
  
  /**
   * Constructs a new IMSApiService object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *  The database connection service.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *  The GuzzleHttp Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *  The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *  The logger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A date time instance.
  */
  public function __construct(
    Connection $connection,
    ClientInterface $httpClient,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $loggerFactory,
    TimeInterface $time,
  ) {
    $this->database = $connection;
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
    $this->time = $time;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   *  Create an invoice.
   *
   *  @param boolean $p_general
   *  Invoice type
   *  @param int $id_sale
   *  Sale ID
   *
   */
  public function create_invoice($p_general, $id_sale) {
    $config = $this->configFactory->get('ims.settings');
    $datos_factura = [];
    // Get payment details
    $ppss_sales = $this->database->select('ppss_sales_details', 'sd');
    $ppss_sales->join('ppss_sales', 's', 'sd.sid = s.id');
    $ppss_sales->condition('sd.id', $id_sale);
    $ppss_sales->fields('sd', ['id', 'created','tax', 'price', 'total', 'description']);
    $ppss_sales->fields('s', ['uid','details']);
    $sales = $ppss_sales->execute()->fetchAssoc();

    $details = json_decode($sales['details']);
    // Get user billing data
    $user = $this->database->select('ims_user_invoice', 'i')
      ->condition('uid', $sales['uid'])
      ->fields('i')
      ->execute()
      ->fetchAssoc();
    // Get the last folio
    $folio = $this->database
      ->query('SELECT max(folio) as folio from {ims_invoices}')
      ->fetchAssoc();
    // Get product data
    $product = \Drupal\node\Entity\Node::load($details->node_id);

    try {
      $conceptos = array();
      $unconcepto = array();
      $base = $sales['price'];
      $importe = $sales['total'];
      $impuesto = $sales['tax'];
      $unconcepto = [
        'ObjetoImp' => '02',
        'ClaveProdServ' => $product->get($config->get('field_prod_serv_code'))->getString(),
        'NoIdentificacion' => '01',
        'Cantidad' => 1,
        'ClaveUnidad' => $product->get($config->get('field_unit_code'))->getString(),
        'Descripcion' => $sales['description'] ?? $details->description,
        'ValorUnitario' => $base,
        'Importe' => $base,
        'Descuento' => 0
      ];
      $impuestosTraslados = array(
        'Base' => $base,
        'Impuesto' => '002',
        'TipoFactor' => 'Tasa',
        'TasaOCuota' => number_format($details->tax / 100, 6),
        'Importe' => $impuesto
      );
      $unconcepto['Impuestos']['Traslados'][0] = $impuestosTraslados;
      $conceptos[] = $unconcepto;

      $datos_factura['Version'] = '4.0';
      $datos_factura['Exportacion'] = '01';
      $datos_factura['Serie'] = $config->get('serie');
      $datos_factura['Folio'] = $folio['folio'] ? $folio['folio'] + 1 : $config->get('folio');
      $datos_factura['Fecha'] = 'AUTO';
      $datos_factura['FormaPago'] = $config->get('c_pago');
      $datos_factura['CondicionesDePago'] = '';
      $datos_factura['SubTotal'] = $base;
      $datos_factura['Descuento'] = null;
      $datos_factura['Moneda'] = strtoupper($details->currency);
      $datos_factura['TipoCambio'] = 1;
      $datos_factura['Total'] = $importe;
      $datos_factura['TipoDeComprobante'] = 'I';
      $datos_factura['MetodoPago'] = 'PUE';
      $datos_factura['LugarExpedicion'] = $config->get('expedition_place');
      // Optional legend: FACTURA, RECIBO, NOTA DE CREDITO, ETC
      $datos_factura['LeyendaFolio'] = 'FACTURA';
      // Payment status - 1: Por Cobrar, 2: Cobrado Parcialmente, 3: Cobrado totalmente
      $datos_factura['EstatusCobranza'] = 3;
      // Total amount charged
      $datos_factura['MontoCobrado'] = $importe;
      // Outstanding balance
      $datos_factura['SaldoInsoluto'] = 0.00;

      // Tax regime
      $datos_factura['Emisor']['RegimenFiscal'] = $config->get('tax_regime');

      if ($p_general) {
        // Required data
        $datos_factura['Receptor']['Rfc'] = 'XAXX010101000';
        $datos_factura['Receptor']['Nombre'] = 'PUBLICO EN GENERAL';
        $datos_factura['Receptor']['UsoCFDI'] = 'S01';
        // Must be the same as LugarExpedición
        $datos_factura['Receptor']['DomicilioFiscalReceptor'] = $config->get('expedition_place');
        $datos_factura['Receptor']['RegimenFiscalReceptor'] = '616';
        // 01-Diaria, 02-Semanal, 03-Quincenal, 04-Mensual o 05-Bimestral
        $datos_factura['InformacionGlobal']['Periodicidad'] = '04';
        $datos_factura['InformacionGlobal']['Meses'] = date('m');
        $datos_factura['InformacionGlobal']['Año'] = date('Y');

      }
      else {
        // Required customer data
        $datos_factura['Receptor']['Rfc'] = $user['rfc'];
        $datos_factura['Receptor']['Nombre'] = $user['name'];
        $datos_factura['Receptor']['UsoCFDI'] = $user['cfdi'];
        $datos_factura['Receptor']['DomicilioFiscalReceptor'] = $user['postal_code'];
        $datos_factura['Receptor']['RegimenFiscalReceptor'] = $user['tax_regime'];

        // Optional customer data
        $datos_factura['Receptor']['Calle'] = $user['address'];
        $datos_factura['Receptor']['NoExt'] = $user['number_ext'];
        $datos_factura['Receptor']['NoInt'] = $user['number_int'];
        $datos_factura['Receptor']['Colonia'] = $user['neighborhood'];
        $datos_factura['Receptor']['Loacalidad'] = $user['city'];
        $datos_factura['Receptor']['Municipio'] = $user['municipality'];
        $datos_factura['Receptor']['Estado'] = $user['state'];
        $datos_factura['Receptor']['Pais'] = 'MEXICO';
        $datos_factura['Receptor']['CodigoPostal'] = $user['postal_code'];
      }

      $datos_factura['Conceptos'] = $conceptos;
      $datos_factura['Impuestos']['TotalImpuestosTrasladados'] = $impuesto;
      $datos_factura['Impuestos']['Traslados'][0]['Base'] = $base;
      // 002 = IVA, 003 = IEPS
      $datos_factura['Impuestos']['Traslados'][0]['Impuesto'] = '002';
      // Tasa, Cuota, Exento
      $datos_factura['Impuestos']['Traslados'][0]['TipoFactor'] = 'Tasa';
      $datos_factura['Impuestos']['Traslados'][0]['TasaOCuota'] = number_format($details->tax / 100, 6);
      $datos_factura['Impuestos']['Traslados'][0]['Importe'] = $impuesto;

      // Call api
      $request = $this->httpClient->request('POST', $config->get('api_endpoint').'/api/v5/invoice/create', [
        'headers' => ['X-Api-Key' => $config->get('api_key')],
        'form_params' => [ 'json' => json_encode($datos_factura)]
      ]);

      $response_body = $request->getBody();
      $data = json_decode($response_body->getContents());
      if ($data->code == '200') {
        // Save all transaction data in DB for future reference.
        $this->database->insert('ims_invoices')->fields([
          'sid' => $sales['id'],
          'folio' => $folio['folio'] ? $folio['folio'] + 1 : $config->get('folio'),
          'uuid' => $data->cfdi->UUID,
          'created' => $data->cfdi->FechaTimbrado,
          'pdf' => $data->cfdi->PDF,
          'xml' => $data->cfdi->XML,
          'p_general' => $p_general ? 1 : 0,
        ])->execute();
        if (!$p_general) {
          // Send invoice email
          $this->sendInvoice($data->cfdi->UUID, $user['mail']);
        }
        return $data;
      }
      else {
        return $data->message ?? t('An error has occurred.');
      }
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $exception = $e->getResponse()->getBody();
        $exception = json_decode($exception);
        return $exception->message ?? t('An error has occurred.');
      }
      else {
        $this->loggerFactory->get('IMS')->error($e->getMessage());
        return $e->getMessage();
      }
    }
  }

  /**
   *  Email the customer their invoice
   *
   *  @param string $uuid
   *  Invoice uuid
   *  @param string $email
   *  User email
   *
   */
  function sendInvoice($uuid, $email){
    $config = $this->configFactory->get('ims.settings');
    try {
      // Send the invoice by mail
      $request = $this->httpClient->request('POST', $config->get('api_endpoint').'/api/v5/invoice/send', [
        'headers' => [
          'X-Api-Key' => $config->get('api_key'),
          'uuid' => $uuid,
          'recipient' => $email,
          'bbc' => '',
          'message' => 'Comprobante Fiscal Digital',
        ],
      ]);
      $response_body = $request->getBody();
      $data = json_decode($response_body->getContents());
      return $data->message;
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $exception = $e->getResponse()->getBody();
        $exception = json_decode($exception);
        return $exception->message ?? t('An error occurred while sending the email');
      }
      else {
        $this->loggerFactory->get('IMS')->error($e->getMessage());
        return t('An error occurred while sending the email.');
      }
    }
  }

  /**
   *  Generate invoices for the general public
   *
   */
  public function globalInvoice() {
    $config = $this->configFactory->get('ims.settings');
    // First day of the current month
    $first_day = strtotime(date('Y-m-01'));
    // Start date and time for invoice generation
    $start_day = strtotime(date('Y-m-t '.$config->get('time').'').$config->get('stamp_date'));
    $today = $this->time->getRequestTime();
    // Last day of the current month
    $last_day = strtotime(date('Y-m-t 23:45:00').$config->get('stamp_date'));
    
    // Validate date and time
    if(($today > $start_day) && ($today < $last_day)) {
      // Get recurring payments that have not been invoiced
      $query = $this->database->select('ppss_sales_details', 's');
      $query->leftJoin('ims_invoices', 'i', 's.id = i.sid');
      $query->condition('s.created', array($first_day, $last_day), 'BETWEEN');
      $query->fields('s', ['id']);
      $query->fields('i',['uuid']);
      $query->range(0, $config->get('num_rows'));
      $num = 0;
      $results = $query->execute()->fetchAll();
      foreach($results as $result) {
        // If there is no invoice
        if(!$result->uuid) {
          // Create invoice
          $invoice = $this->create_invoice(true, $result->id);
          if($invoice->code && $invoice->code == '200') {
            // Count invoices
            $num = $num + 1;
          }
          else {
            $this->loggerFactory->get('IMS')->info(t('Error when creating sales invoice 
            @id - @invoice', [
              '@id' => $result->id,
              '@invoice' => $invoice,
            ]));
          }
        }
      }
      if($num > 0) {
        $this->loggerFactory->get('IMS')->info(t('@num invoices were generated for the 
        general public', [
          '@num' => $num
        ]));
      }
    }
  }
}
