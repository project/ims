<?php

namespace Drupal\ims\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\ims\IMSApiService;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Define InvoiceController class.
 *
 */
class InvoiceController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
  */
  protected $configFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current request service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $currentRequest;

  /**
   * The api Service
   *
   * @var \Drupal\ims\IMSApiService
   */
  protected $imsApiService;

  /**
   * The form builder interface
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * InvoiceController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The current request service.
   * @param \Drupal\ims\IMSApiService $imsApiService
   *   The api Service.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder interface.
   */

  public function __construct(
    Connection $database,
    LoggerChannelFactory $loggerFactory,
    MessengerInterface $messenger,
    RequestStack $requestStack,
    IMSApiService $imsApiService,
    FormBuilderInterface $formBuilder,
  ) {
    $this->database = $database;
    $this->loggerFactory = $loggerFactory;
    $this->messenger = $messenger;
    $this->currentRequest = $requestStack;
    $this->imsApiService = $imsApiService;
    $this->formBuilder = $formBuilder;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('ims.api_service'),
      $container->get('form_builder'),
    );
  }

  /**
   *  Create a payment invoice.
   *
   * @param int $id
   *   Payment id
   */
  public function create_invoice($id) {
    $config = $this->config('ims.settings');
    $api_key = $config->get('api_key');
    $api_endpoint = $config->get('api_endpoint');
    if (!(empty($api_key) || empty($api_endpoint))) {
      // Check if payment exists
      $ppss_sales = $this->database->select('ppss_sales_details', 'payments');
      $ppss_sales->join('ppss_sales', 'sales', 'payments.sid = sales.id');
      $ppss_sales->condition('payments.id', $id);
      $ppss_sales->condition('uid', $this->currentUser()->id());
      $ppss_sales->fields('payments', ['id', 'created']);
      $payment = $ppss_sales->execute()->fetchAssoc();
      $message = '';
      if ($payment) {
        $invoice = $this->database->select('ims_invoices', 'i')
          ->condition('sid', $id)
          ->fields('i')
          ->execute()
          ->fetchAssoc();
        // Check if an invoice already exists
        if ($invoice) {
          $message = '<h5>'.$this->t('Your invoice is already generated').'</h5>';
        }
        else {
          // Validate the purchase date
          // First day of the current month 
          $first_day = strtotime(date('Y-m-01'));
          // Last day of the current month
          $last_day = strtotime(date('Y-m-t 23:59:00'));
          // Validate payment date
          if ($payment['created'] >= $first_day && $payment['created'] < $last_day) {
            // Validate that user billing data exists
            $user = $this->database->select('ims_user_invoice', 'i')
              ->condition('uid', $this->currentUser()->id())
              ->fields('i')
              ->execute()
              ->fetchAssoc();
            $url = Url::fromRoute('ims.user_data_form', [
              'user' => $this->currentUser()->id()
            ]);
            if ($user) {
              // Call service create_invoice
              $invoice = $this->imsApiService->create_invoice(FALSE, $payment['id']);
              // Service response
  
              if (is_object($invoice) && $invoice->code == '200') {
                // Dsiplay invoice data
                $pdf = $invoice->cfdi->PDF;
                $xml = $invoice->cfdi->XML;
                $uuid = $invoice->cfdi->UUID;
                $created = $invoice->cfdi->FechaTimbrado;
                $message = $this->t('<h5>Invoice generated successfully </h5><p>
                Folio UUID: @uuid<br>Creation date: @created<br><a href=@pdf 
                target="_blank">View PDF</a>&nbsp;<a href=@xml target="_blank">
                Download XML</a></p>', [
                  '@uuid' => $uuid,
                  '@created' => $created,
                  '@pdf' => $pdf,
                  '@xml' => $xml
                ]
              );

              }
              else {
                $this->messenger->addError($invoice);
                $message = '<b>Error: </b>'. $this->t('Make sure you have valid 
                tax information, please check <a href=@url>here</a>.',[
                  '@url' => $url->toString()
                ]);
              }
            }
            else {
              $this->messenger->addError($this->t('Your billing information is missing'));
              $message = $this->t('Please register your <a href=@url>tax 
              information here.</a>', ['@url' => $url->toString()]);
            }
          }
          else {
            $this->messenger->addWarning($this->t('You cannot generate 
            invoices from previous dates. Please contact with the site 
            administrator'));
          }
        }
      }
      else {
        $this->messenger->addWarning($this->t('Access denied'));
        $message = $this->t('You are not authorized to access this page.');
      }
    }
    else {
      $message = "<b>Error: </b>IMS module don't has configured properly, 
      please review your settings.";
      $this->loggerFactory->get('IMS')->alert($message);
    }
    return [
      '#type' => 'markup',
      '#markup' => $message
    ];
  }

  /**
   * Create a payment receipt.
   *
   * @param int $user
   *  User id
   * @param int $id
   *  Payment id
   */
  public function receipt($user, $id){
    $user_id = $this->currentUser()->hasPermission('access user profiles') ? $user : $this->currentUser()->id();
    // Get recurring payment details
    $ppss_sales = $this->database->select('ppss_sales_details', 'payments');
    $ppss_sales->join('ppss_sales', 'sales', 'payments.sid = sales.id');
    $ppss_sales->condition('payments.id', $id);
    $ppss_sales->condition('uid', $user_id);
    $ppss_sales->fields('payments', ['id', 'created', 'tax', 'price', 'total', 'description']);
    $ppss_sales->fields('sales', ['id', 'mail', 'platform', 'details']);
    $sales = $ppss_sales->execute()->fetchAssoc();
    $user_name='';

    if ($sales) {
      $details = json_decode($sales['details']);
      if ($sales['platform'] == 'Stripe') {
        $user_name = $details->customer_details->name;
      }
      $data = [
        'folio' => $sales['id'],
        'email' => $sales['mail'],
        'platform' => $sales['platform'],
        'created' => date('d/m/Y', $sales['created']),
        'product' => $sales['description'] ?? $details->description,
        'price' => $sales['price'],
        'total' => $sales['total'],
        'iva' => $sales['tax'],
        'user' => $user_name,
      ];
      return [
        '#theme' => 'receipt',
        '#sale' => $data,
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Without data to show')
      ];
    }
  }

  /**
   * Sales listing.
   *
   */
  public function list_sales() {
    $request = $this->currentRequest->getCurrentRequest();
    $form['form'] = $this->formBuilder->getForm('Drupal\ims\Form\FilterTableForm');
    $start_date = strtotime($request->query->get('start_date') ?? date('Y-m-01'));
    $end_date = strtotime($request->query->get('end_date') ?? date('Y-m-t'));
    // Create table header
    $header_table = [
      'folio' => $this->t('Folio'),
      'id' => $this->t('Subscription ID'),
      'name' => $this->t('Plan'),
      'total' => $this->t('Total price'),
      'platform' => $this->t('Payment type'),
      'date' => $this->t('Date'),
      'user' => $this->t('Email'),
      'invoice' => $this->t('Invoice'),
      'type' => $this->t('Type'),
      'event' => $this->t('Event ID'),
    ];
    // Get all payments from ppss_sales_details
    $query = $this->database->select('ppss_sales', 'sales');
    $query->join('ppss_sales_details', 'payments', 'sales.id = payments.sid');
    $query->leftJoin('ims_invoices', 'invoices', 'payments.id = invoices.sid');
    $query->leftJoin('ims_user_invoice', 'user_invoice', 'sales.uid = user_invoice.uid');
    $query->condition('payments.created', [$start_date, $end_date], 'BETWEEN');
    $query->fields('sales', ['platform', 'details', 'id_subscription', 'frequency']);
    $query->fields('payments', ['id','total', 'created', 'event_id', 'description']);
    $query->fields('invoices', ['uuid','p_general']);
    $query->fields('user_invoice', ['rfc', 'mail']);
    $query->orderBy('id', 'DESC');
    $results = $query->execute()->fetchAll();

    $rows = [];
    foreach ($results as $data) {
      $sale = json_decode($data->details);
      // Print the data from table
      $rows[] = [
        'folio' => $data->id,
        'id' => $data->id_subscription,
        'name' => ($data->description ?? $sale->description) .' / '. $data->frequency,
        'total' => number_format($data->total, 2, '.', ','),
        'platform' => $data->platform,
        'date' => date('d/m/Y', $data->created),
        'user' => $data->mail,
        'invoice' => $this->t($data->uuid ? 'Invoiced': 'Pending'),
        'type' => $data->p_general ? $this->t('General public') : $data->rfc,
        'event' => $data->event_id,
      ];
    }
    // Display data in site
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => $this->t('No data')
    ];
    return $form;
  }

  /**
   * Show purchase details by id sales.
   *
   * @param int $user
   *  User ID
   * @param int $id
   * Sale ID
   *
   */
  public function show_purchase($user, $id) {
    $user_id = $this->currentUser()->hasPermission('access user profiles') ? $user : $this->currentUser()->id();
    $sales = $this->database->select('ppss_sales', 's')
      ->condition('id', $id)->condition('uid', $user_id)
      ->fields('s', ['id','uid','mail','platform','details','created', 'status', 
      'id_subscription', 'frequency', 'expire'])
      ->execute()
      ->fetchAssoc();
    // If exist sale
    if ($sales) {
      // Get details sale
      $details = json_decode($sales['details']);
      // Get listining recurring payments by id sale
      $payments_query = $this->database->select('ppss_sales_details', 'payments');
      $payments_query->leftJoin('ims_invoices', 'invoices', 'payments.id = invoices.sid');
      $payments_query->condition('payments.sid', $sales['id']);
      $payments_query->fields('payments', ['id', 'total', 'created', 'tax', 'description']);
      $payments_query->fields('invoices', ['uuid', 'p_general']);
      $payments_query->orderBy('payments.id', 'DESC');
      $payments = $payments_query->execute()->fetchAll();
      $url_cancel = '';
      // Cancellation url
      if ($sales['platform'] == 'Stripe') {
        $url_cancel = Url::fromRoute('stripe_gateway.cancel_subscription', [
          'user' => $user, 'id' => $sales['id']
        ], []);
      }

      $data = [
        'id' => $sales['id'],
        'subscription' => $sales['id_subscription'],
        'email' => $sales['mail'],
        'status' => $sales['status'] ? 'ACTIVE' : 'INACTIVE',
        'platform' => $sales['platform'],
        'created' => date('d/m/Y', $sales['created']),
        'frequency' => $sales['frequency'],
        'product' => $payments[0]->description ??  $details->description,
        'total' => $payments[0]->total,
        'payments' => $payments,
        'cancel' => $sales['status'] && $sales['expire'] == null ?
          Link::fromTextAndUrl($this->t('Cancel'), $url_cancel) : '',
        'last_pay' => $payments[0]->created ?? '',
        'expire' => $sales['expire'],
        'user' => $user_id
      ];
      // Show data in template
      return [
        '#theme' => 'purchase-details',
        '#sale' => $data,
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Without data to show')
      ];
    }
  }

  /**
   *  Show generated invoice.
   *
   * @param int $user
   *  User ID
   * @param int $id
   *  Sale ID
   */
  public function show_invoice($user, $id) {
    $invoice = $this->database->select('ims_invoices', 'i')
      ->condition('uuid', $id)
      ->fields('i')
      ->execute()
      ->fetchAssoc();

    $uuid = $invoice['uuid'];
    $pdf = $invoice['pdf'];
    $xml = $invoice['xml'];
    $created = $invoice['created'];
    return [
      '#type' => 'markup',
      '#markup' => '<h5>'.$this->t('Billing details').'</h5>' .
      $this->t('<p>Folio UUID: @uuid<br>Creation date: @created<br>
      <a href=@pdf target="_blank">View PDF</a>&nbsp;<a href=@xml
      target="_blank">Download XML</a></p>', [
        '@uuid' => $uuid,
        '@created' => $created,
        '@pdf' => $pdf,
        '@xml' => $xml
      ])
    ];
  }
}

