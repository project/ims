# Invoicing Mexican Sales (IMS)

## Introduction

This module allows site to invoicing in compliance with mexican laws service. By now, module only works with Factura Digital México service to generate invoices.

## Requirements

This module requires the following modules:
[Drupal module PPSS](https://www.drupal.org/project/ppss)
[Drupal module Stripe Gateway](https://www.drupal.org/project/stripe_gateway)

## Installation

- Install as you would normally install a contributed Drupal module. See [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for more details.

## Configuration

In Factura Digital service

- Register to [Factura digital](https://www.facturadigital.com.mx/) if you haven't yet.
- Enter your information in Configuracion >> Mis datos fiscales
- Create your folios and series in Configuracion >> Folios y series
- Upload your Digital Seal Certificates in Configuracion >> Certificados CSD

In Drupal main menu go to: Configuration » Invoicing Mexican Sales

- Enter **endpoint URL**

   If you are in development mode enter [https://app.facturadigital.com.mx](https://app.facturadigital.com.mx)

   If your are in sandbox mode enter [https://sandbox-app.facturadigital.com.mx](https://sandbox-app.facturadigital.com.mx)

- Enter **API Key**

   To obtain the API Key you must logint with your account at [https://app.facturadigital.com.mx](https://sandbox-app.facturadigital.com.mx)

- Enter **Tax regime**
- Enter **Expedition place**
- Enter **Serie** and **Folio**

   You must generate your folio and series in your Factura digital account.

- Select **Payment Method**

This method apply to all your products.

- Enter **Number of records**

   Number of records to be billed each time the cron is executed.

- Enter **Invoicing time interval**

   Time interval in days before end of month to invoice general public sales.

- Enter **Hour**

   Default time that will be used to start General Public Billing

Fields configuration:
- Enter the **Product or service code field name**

   Name of the field to store product or service code SAT.

- Enter the **Unit code field name**

   Name of the field to store unit code SAT.

- Enable permissions:

   View IMS Block

- Add IMS Invoicing Block

## How it works

This module allows you to generate invoices for each payment made, to generate the invoice the user must register their billing information.

By Now, we only use [Factura digital](https://www.facturadigital.com.mx/) API to generate invoices.
